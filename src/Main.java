import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

/**
 * Created by omfg on 10.12.16.
 */
public class Main {
    private static final String URL = "jdbc:mysql://192.168.1.106:3306/mydbtest";
    private static final String USERNAME = "omfg";
    private static final String PASSWORD = "101541";


    public static void main(String[] args) {


        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);




        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Fail to load driver");
        }
        try (Connection connection = DriverManager.getConnection(URL,USERNAME,PASSWORD); Statement statement = connection.createStatement()){
//           statement.execute("INSERT INTO animal(anim_name,anim_desc) VALUES('name','desc');");
//           int res = statement.executeUpdate("UPDATE animal SET anim_name= 'New_name' WHERE id = 1 ");
//            System.out.println(res);
//           ResultSet resultSet =  statement.executeQuery("SELECT * FROM animal");
            statement.addBatch("INSERT INTO animal(anim_name,anim_desc) VALUES('name1','desc');");
            statement.addBatch("INSERT INTO animal(anim_name,anim_desc) VALUES('name2','desc');");
            statement.addBatch("INSERT INTO animal(anim_name,anim_desc) VALUES('name3','desc');");

            statement.executeBatch();
            statement.clearBatch();//очистка бэч
            boolean status = statement.isClosed();
            System.out.println(status);

            statement.getConnection();//получить подключение

            statement.close();//закрыть подключение
        }catch (SQLException e){e.printStackTrace();}

    }
}
